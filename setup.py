import codecs
import os
import re
import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


def read(*parts):
    """
    Build an absolute path from *parts* and and return the contents of the
    resulting file.  Assume UTF-8 encoding.
    """
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, *parts), "rb", "utf-8") as f:
        return f.read()


def find_version(*file_paths):
    """
    Build a path from *file_paths* and search for a ``__version__``
    string inside.
    """
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")



setup(
    name='dashing',
    version=find_version('dashing', '__init__.py'),
    description='doc2dash wrapper to ease Sphinx doc to Dash docset conversion',
    long_description=read('README.rst'),
    license='MIT',
    author='Lucas Taylor',
    author_email='ltaylor.volks@gmail.com',
    url='http://github.com/ltvolks/dashing/',
    packages=find_packages(),
    entry_points={
    'console_scripts': [
        'dashing = dashing.__main__:main'
    ]},
    classifiers=[
    'Development Status :: 3 - Alpha',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.2',
    'Programming Language :: Python :: 3.3',
    'Programming Language :: Python :: 2',
    'Programming Language :: Python :: 2.7',
    'Environment :: Console',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: MIT License',
    'Operating System :: MacOS :: MacOS X',
    'Topic :: Documentation',
    'Topic :: Software Development',
    'Topic :: Software Development :: Documentation',
    'Topic :: Text Processing',
    ],
    test_suite='',
    install_requires=[
        'click>=4.0',
        'Sphinx'
    ],
)
