from __future__ import absolute_import, division, print_function
"""
Wrapper around doc2dash to automate building of dash docs for editable
pip-installed python projects.
"""
import errno
import logging
import logging.config
import os
from os.path import abspath, dirname, exists, join
import re
import subprocess
import sys

import click

from . import __version__

log = logging.getLogger('dashing')

DEFAULT_DOCSET_PATH = os.path.expanduser(
    '~/Library/Application Support/doc2dash/DocSets'
)



class ClickEchoHandler(logging.Handler):
    """
    Use click.echo() for logging.  Has the advantage of stripping color codes
    if output is redirected.  Also is generally more predictable.
    """
    _level_to_fg = {
        logging.ERROR: "red",
        logging.WARN: "yellow",
    }

    def emit(self, record):
        click.echo(click.style(
            record.getMessage(),
            fg=self._level_to_fg.get(record.levelno, "reset")
        ), err=record.levelno >= logging.WARN)


def create_log_config(verbose, quiet):
    """
    We use logging's levels as an easy-to-use verbosity controller.
    """
    if verbose and quiet:
        raise ValueError(
            "Supplying both --quiet and --verbose makes no sense."
        )
    elif verbose:
        level = logging.DEBUG
    elif quiet:
        level = logging.ERROR
    else:
        level = logging.INFO
    return {
        "version": 1,
        "formatters": {
            "click_formatter": {
                "format": '%(message)s',
            },
        },
        "handlers": {
            "click_handler": {
                'level': level,
                'class': 'dashing.__main__.ClickEchoHandler',
                'formatter': 'click_formatter',
            }
        },
        "loggers": {
            "dashing": {
                "handlers": ["click_handler"],
                "level": level,
            },
        }
    }


@click.command()
@click.argument('project')
@click.option('--venv', envvar='VIRTUAL_ENV', type=click.Path(exists=True))
@click.option('--name', help="Name of docset (default=project)")
@click.option('--relax-warnings', is_flag=True, help="Turn off warnings-as-errors when building Sphinx docs")
@click.option(
    "--index-page", "-I", metavar="FILENAME", type=click.Path(), default='index.html',
    help="Set the file that is shown when the docset is clicked within "
    "Dash.app."
)
@click.option('--builddir', envvar='BUILDDIR', default='_build', type=click.Path(),
    help="Set the Sphinx build directory for finished output. Projects "
    "that respect the BUILDDIR environmental variable will use this "
    "directory, otherwise this should specify the hardcoded BUILDDIR in "
    "the project's conf.py")
@click.option(
    "--icon", "-i", type=click.File("rb"), help="Add PNG icon to docset."
)
@click.option(
    "--quiet", "-q", is_flag=True, help="Limit output to errors and warnings."
)
@click.option(
    "--verbose", "-v", is_flag=True, help="Be verbose."
)
@click.version_option(version=__version__)
def main(project, venv, builddir, icon, index_page, name, relax_warnings, verbose, quiet):
    """
    Dashing - Use doc2dash to automate building of dash docs for
    editable pip-installed python projects.
    """
    try:
        logging.config.dictConfig(
            create_log_config(verbose=verbose, quiet=quiet)
        )
    except ValueError as e:
        click.secho(e.args[0], fg="red")
        raise SystemExit(1)

    src = join(venv, 'src')
    project_base = join(src, project)

    if not exists(project_base):
        log.error("'{0}' project does not exist in {1}".format(project, click.format_filename(project_base)))
        if exists(src):
            projects = [p for p in sorted(os.listdir(src))
                        if os.path.isdir(os.path.abspath(join(src, p)))]
            if projects:
                log.info("Available src packages:\n{}".format('\n'.join(projects)))

        raise SystemExit(errno.ENOENT)

    src_candidates = [
        join(project_base, 'doc', 'build'),
        join(project_base, 'docs', 'build'),
        join(project_base, 'docs'),
        join(project_base, 'doc'),
        project_base,
    ]

    makefile_found = False
    # build directory relative to Makefile
    # Not all Sphinx projects respect BUILDDIR, but we'll pass it through
    env = os.environ.copy()
    BUILDDIR = env.setdefault('BUILDDIR', builddir)

    for src in src_candidates:
        if exists(join(src, 'Makefile')):
            try:
                log.info("Attempting to run 'make html' for {0} docs".format(project))
                args = ['make', 'clean', 'html']

                # Reset SPHINXOPTS to disable the -W warning-to-errors flag
                if relax_warnings:
                    args.append('SPHINXOPTS=')

                build_results = subprocess.check_output(args, cwd=src, env=env)
            except subprocess.CalledProcessError:
                log.error("Error running 'make html' on source docs in {0}".format(src))
                raise SystemExit(1)
            else:
                makefile_found = True
                pat = '^Build finished. The HTML pages are in (.+)\.$'
                match = re.search(pat, build_results, flags=re.MULTILINE)
                if match is not None:
                    built = join(src, match.group(1))
                    log.info("Using built docs in {}".format(built))
                else:
                    built = join(src, BUILDDIR, 'html')
                    log.error("Expected Sphinx output to include location of "
                              "built HTML pages, but it did not. "
                              "Will try {}".format(built))
                break
        continue

    if not makefile_found:
        log.error("Could not find Makefile in any of candidate src directories:\n{0}".format('\n'.join(src_candidates)))
        raise SystemExit(1)

    if not exists(built):
        log.error("No built html docs found for {0} in {1}.".format(project, built))
        raise SystemExit(1)

    verbosity = None
    if quiet:
        verbosity = '-q'
    elif verbose:
        verbosity = '-v'

    doc2dash_args = ['doc2dash', '-f', '-A', '--name', name or project]
    if verbosity is not None:
        doc2dash_args.append(verbosity)

    # Specify an index doc and icon
    if exists(join(built, index_page)):
        doc2dash_args.extend(['--index-page', index_page])
    if icon is not None and exists(join(built, icon)):
        doc2dash_args.extend(['--icon', icon])

    doc2dash_args.append(built)
    try:
        subprocess.check_output(doc2dash_args, cwd=src, env=env)
    except subprocess.CalledProcessError as e:
        log.error(e.output)
        raise SystemExit(1)
    except OSError as e:
        log.error("Could not execute doc2dash (is it installed?)")
        raise SystemExit(1)
    else:
        log.info("'{0}'' docs built and added to Dash as {1}".format(project, name or project))
