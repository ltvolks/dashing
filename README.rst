**************************************
Dashing: Generate docsets for Dash.app
**************************************

Utility that wraps the `doc2dash`_ utility to automate the building of
`Dash.app`_  compatible documentation sets from editable ``pip``-installed
python projects.

The idea is to dedicate a virtualenv to install editable packages and
build the documentation automatically based on a few assumptions:

1. Doc2Dash is installed as a utility, e.g. installed using `pipsi`_ to
   isolate it and its dependencies
2. The virtualenv directory is named by the VIRTUAL_ENV environmental
   variable.
3. Packages are installed using ``pip's`` VCS editable install support in
   the default virtualenv src directory, e.g. ``<venv path>/src/SomeProject``

.. code-block:: shell

    $ pip install -e git+git@github.com:Pylons/colander.git#egg=colander


.. note:: Ensure you follow instructions for building docs for the
    project, in case there are any special requirements like sphinx
    extensions or themes (`pylons_sphinx_theme`_, `sphinx_rtd_theme`_)
    that are not automatically installed by the project.



Install
=======

Install Doc2Dash
----------------
`doc2dash`_ should be installed and on your path. A sane way to do this
is to install it as a utility using `Pipsi`_. `Pipsi`_ is a wrapper
around virtualenv and `pip`_ that makes it easy to install python
packages as "utilities" without worrying about version conflicts or
cluttering up system or project python environments.

`pipsi`_ installs each package into ``~/.local/venvs/PKGNAME`` and then
symlinks all new scripts into ``~/.local/bin``.

.. code-block:: shell

    $ pipsi install doc2dash


Install Dashing
---------------

Create a virtualenv for dashing and install using pip

.. code-block:: shell

    $ virtualenv dashing
    $ source dashing/bin/activate
    (dashing) $ pip install dashing


Usage
=====

Now install some editable packages that contain Sphinx documentation:

.. code-block:: shell

    (dashing) $ pip install -e git+git@github.com:itamarst/crochet.git#egg=crochet
    (dashing) $ dashing crochet


Docs Requirements
------------------

Some packages have special requirements like themes or Sphinx extensions.
If the packages include setuptools extras for docs, you can automatically
install the doc requirements by appending the extras.

For instance, `colander`_ requires the `pylons_sphinx_theme`_, `docutils`_,
and a minimum `Sphinx`_ version. To install the extras automatically:

.. code-block:: shell

    (dashing) $ pip install -e git+git@github.com:Pylons/colander.git#egg=colander[docs]
    (dashing) $ dashing colander

If you don't, you'll get an error when building which is easily solved
by manually installing the requirements.


Advanced
========

Since you're dealing with source checkouts, you may want to generate and
add documentation for specific versions of a project. In this case, you
will need to checkout the tag or version you want to generate docs
against.

.. warning:: Switching checkouts and executing any python code in a
             project may leave incompatible ``*.pyc`` files. It is a
             good idea to clear these out and in general reset the state
             of the repo (e.g. by running ``git clean``).

You can provide a ``--name`` argument to differentiate the docset in Dash:

.. code-block:: shell

    (dashing) $ cdvirtualenv src
    (dashing) $ cd pyramid
    (dashing) $ git co 1.5.7
    (dashing) $ git clean -xf
    (dashing) $ pip install -e .
    (dashing) $ dashing --name "Pyramid 1.5.7" pyramid


TODO
====

* Invoke ``sphinx-build`` directly for more control
* Find and include relevant icons for docsets (16x16 and 32x32)
* Tar the docset for `Dash User Contributions`_
* Create/edit a ``docset.json`` for `Dash User Contributions`_


.. _colander: https://github.com/Pylons/colander
.. _Dash.app: http://kapeli.com/dash/
.. _Dash User Contributions: https://github.com/Kapeli/Dash-User-Contributions
.. _doc2dash: https://doc2dash.readthedocs.org/
.. _docutils: http://docutils.sourceforge.net/
.. _pip: https://pip.pypa.io/en/latest/index.html
.. _pipsi: https://github.com/mitsuhiko/pipsi
.. _pylons_sphinx_theme: https://github.com/Pylons/pylons_sphinx_theme
.. _Sphinx: http://sphinx-doc.org/
.. _sphinx_rtd_theme: https://github.com/snide/sphinx_rtd_theme
